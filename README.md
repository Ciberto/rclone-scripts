# Updates:
### 2020/04/05:
   * **reporter_v2.sh:**
     - Added compatibility with api errors (experimental).

### 2020/02/11:
   * **reporter_v2.sh:**
     - Added compatibility with rclone v1.51 and her non log empty "Errors: 0" log.

### 2019/05/21:
   * **upload_rclone_v2.sh:**
     - "-t" flag has been deprecated and will be removed on a following version
     - The "-n" flag (notifications) use a "reporter_v2.sh" script and they has get the same format (this include the error checker).
     - The notifications use the "-t" on "reporter_v2.sh".

   * **reporter_v2.sh:**
     * Now the logs list will get automatically. You don't will specify the disk list in the script never more.
       - **How work this?:**

        > *Assuming that your files has the correct structure: `[prefix]_[diskname]_[category-name].log`*
         1. Scan the log path.
         2. Recognize the files that begin with the [prefix]
         3. Then the script cut the log name and only get the second value ([diskname]).
         4. Finally the script use the 'unicq' command to get the finally list.
     * When the script detects any error on the log, the notification will show the errors.

#### First Commit:
 - N/A


# Help:

You can use `-h` flag in both scripts to get more information.

 - `bash upload_rclone_v2.sh -h`
 - `bash reporter_v2.sh -h`