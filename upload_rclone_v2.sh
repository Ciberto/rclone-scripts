#!/bin/bash
function help {
echo -e "\e[34mFlags:"
echo -e "-----------------\e[0m"
echo "-h help"
echo "-s SOURCE (required)"
echo "-d DESTINATION (required)"
echo "-L Log path (optional) default is /tmp/drive_backup"
echo "-l Log file name (required) "
echo "-n Enable Telegram Notification (optional)"
echo "-t Telegram titleTHIS OPTION HAS BEEN DEPRECATED! Will be removed on a following versions"
echo ""
echo -e "\e[34mCommand example: (Without Telegram Notify)"
echo -e "-----------------\e[0m"
echo 'upload_rclone.sh -s "/Downloads" -d "gsuite-Series:/one/folder/Series" -L "/tmp/drive_backup" -l "disk1_Series.log"'
echo ""
echo -e "\e[34mCommand example: (With Telegram Notify)"
echo -e "-----------------\e[0m"
echo 'upload_rclone.sh -s "/Downloads" -d "gsuite-Series:/one/folder/Series" -L "/tmp/drive_backup" -l "disk1_Series.log" -n'
echo ""
echo -e "\e[34mBinaries locations:"
echo -e "-----------------\e[0m"
echo "In this script, rclone binary is localized in \"/usr/bin/rclone\""
echo "In this script, telegram-notify binary is localized in \"/usr/sbin/telegram-notify\""
echo ""
echo -e "\e[33mInfo:"
echo -e "-----------------\e[0m"
echo "rClone: "
echo " - Web: https://rclone.org"
echo ""
echo "Telegram-notify: "
echo " - Download: https://github.com/NicolasBernaerts/debian-scripts/blob/master/telegram/telegram-notify"
echo " - Documentation: http://www.bernaerts-nicolas.fr/linux/75-debian/351-debian-send-telegram-notification"
echo ""
echo -e "\e[33mLog Name details:"
echo -e "-----------------\e[0m"
echo 'If you has planned use the "reporter" script, is very recommended use this name structure'
echo ""
echo -e "  - [prefix]_[diskname]_[category-name].log"
echo -e "  - rclone_red1_terror-movies.log"

}

LOG_PATH="/tmp/drive_backup"

s_flag=0
d_flag=0
L_flag=0
l_flag=0
n_flag=0
t_flag=0

while getopts ":s:d:L:l:nth" opts; do
   case ${opts} in
      h) help; exit 1 ;;
      s) SOURCE=${OPTARG}; s_flag=1 ;;
      d) DESTINATION=${OPTARG}; d_flag=1 ;;
      L) LOG_PATH=${OPTARG}; L_flag=1 ;;
      l) LOG_FILE=${OPTARG}; l_flag=1 ;;
      n) n_flag=1 ;; 
      t) TITLE=${OPTARG}; e_flag=1 ;;
     \?) echo "UPS!, wrong option: -$OPTARG" ;;
      :) echo "UPS!, The -$OPTARG option has not any value"; exit 1 ;;
   esac
done


if [ $s_flag -eq 0 ]; then
	echo -e "\e[31mERROR:"
	echo -e "-----------------\e[0m"
	echo "The -s option is required."
	echo ""
	help
	exit 1
fi

if [ $d_flag -eq 0 ]; then
	echo -e "\e[31mERROR:"
	echo -e "-----------------\e[0m"
	echo "The -d option is required."
	echo ""
	help
	exit 1
fi

if [ $l_flag -eq 0 ]; then
	echo -e "\e[31mERROR:"
	echo -e "-----------------\e[0m"
	echo "The -l option is required."
	echo ""
	help
	exit 1
fi

## Create log folder if not exists
mkdir -p "$LOG_PATH"

## Delete latest log
rm -f $LOG_PATH/$LOG_FILE

/usr/bin/rclone \
	copy \
	--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0" \
	--ignore-existing \
	--log-file=$LOG_PATH/$LOG_FILE \
	--log-level INFO \
	--stats 60m \
	--stats-file-name-length 0 \
	--no-update-modtime \
	--transfers 4 \
	--checkers 8 \
	--contimeout 60s \
	--timeout 300s \
	--retries 3 \
	--low-level-retries 10 \
	"$SOURCE" "$DESTINATION"

if [ $n_flag -eq 1 ]; then
	SCRIPT_PATH=$(readlink -f $0)
	SCRIPT_PATH=$(dirname $SCRIPT_PATH)
	bash $SCRIPT_PATH/reporter_v2.sh -s "$LOG_PATH/$LOG_FILE" -t
fi
