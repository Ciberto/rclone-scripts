#!/bin/bash
function help {
echo -e "\e[34mFlags:"
echo -e "-----------------\e[0m"
echo "-h Help"
echo '-p Prefix name (optional) default is "rclone_ "'
echo "-L Log path (optional) default is /tmp/drive_backup"
echo "-t Shows modification date of the log file."
echo "-s Single log (this flag is used by upload script)."
echo ""
echo -e "\e[34mCommand example:"
echo -e "-----------------\e[0m"
echo 'reporter_v2.sh -L "/tmp/tmp/drive_backup"'
echo ""
echo -e "\e[34mBinaries locations:"
echo -e "-----------------\e[0m"
echo "In this script, telegram-notify binary is localized in \"/usr/sbin/telegram-notify\""
echo ""
echo -e "\e[33mInfo:"
echo -e "-----------------\e[0m"
echo "Telegram-notify: "
echo " - Download: https://github.com/NicolasBernaerts/debian-scripts/blob/master/telegram/telegram-notify"
echo " - Documentation: http://www.bernaerts-nicolas.fr/linux/75-debian/351-debian-send-telegram-notification"
echo ""
}

function details {
        echo "  - $ICON *$CATEGORY_NAME*" >> $REPORT_PATH/$REPORT_FILE
        if [ $t_flag -eq 1 ]; then
          echo "Date: \`$DATE\`" >> $REPORT_PATH/$REPORT_FILE
        fi        
        #echo '```' >> $REPORT_PATH/$REPORT_FILE
        echo $TRANSFERIDO  >> $REPORT_PATH/$REPORT_FILE
        if [ $error_flag -eq 1 ]; then
             echo $ERRORS >> $REPORT_PATH/$REPORT_FILE
        fi  
        echo $DURACION  >> $REPORT_PATH/$REPORT_FILE 
        #echo '```' >> $REPORT_PATH/$REPORT_FILE
}

function process_log {

        echo "$LOG" > $REPORT_PATH/$TEMP_FILENAME
        CATEGORY_NAME=$(cat $REPORT_PATH/$TEMP_FILENAME)
        CATEGORY_NAME=$(basename $CATEGORY_NAME)
        CATEGORY_NAME=$(echo "$CATEGORY_NAME" | cut -d "_" -f 3)

        ERRORS=$(tail -n 6 $LOG |   grep "Errors:")
        ERROR_CHECKER=$(tail -n 6 $LOG |   grep "Errors:")
        TRANSFERIDO=$(tail -n 6 $LOG |   grep "ytes")
        DURACION=$(tail -n 6 $LOG |   grep "time")
        DATE=$( stat --printf='%y' $LOG | cut -d "." -f 1)

        if [ -z "$ERROR_CHECKER" ] || [ "$ERROR_CHECKER" == "Errors:                 0" ] && [ ! -z "$TRANSFERIDO" ]
        then
                ICON=$(echo -e "\U2705")
                details
        else
                ICON=$(echo -e "\U274C")
                error_flag=1
                details
        fi
}


LOGS_PATH="/tmp/drive_backup"
LOGS_PREFIX="rclone_"
REPORT_PATH="$LOGS_PATH/reporter"
REPORT_FILE="report.log"
TEMP_FILENAME="logname.tmp"
DISKS=$(ls -1 "$LOGS_PATH" | grep "$LOGS_PREFIX" | cut -d "_" -f 2 | uniq )

p_flag=0
L_flag=0
t_flag=0
s_flag=0
error_flag=0

while getopts "L:ths:" opts; do
   case ${opts} in
      h) help; exit 1;;
      p) LOGS_PREFIX=${OPTARG}; p_flag=1 ;;
      L) LOG_PATH=${OPTARG}; L_flag=1 ;;
      t) t_flag=1 ;;
      s) LOG=${OPTARG}; s_flag=1 ;;
     \?) echo "UPS!, wrong option: -$OPTARG" ;;
      :) echo "UPS!, The -$OPTARG option has not any value"; exit 1 ;;
   esac
done

mkdir -p $REPORT_PATH
rm -f $REPORT_PATH/$REPORT_FILE

if [ $s_flag -eq 0 ]; then
  for DISK in $DISKS
  do
          echo "---------------"  >> $REPORT_PATH/$REPORT_FILE
          echo "*$DISK*" >> $REPORT_PATH/$REPORT_FILE
          echo "---------------"  >> $REPORT_PATH/$REPORT_FILE
          LOGS=$(ls $LOGS_PATH/$LOGS_PREFIX$DISK*)

          for LOG in $LOGS
          do
                  process_log
          done
  done
else 
  process_log
fi


TELEGRAM_TEXT=$(cat $REPORT_PATH/$REPORT_FILE)

/usr/sbin/telegram-notify --silent --text "$TELEGRAM_TEXT"